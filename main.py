import pandas as pd
import math


teamSize = int(input())

mainFileName = 'student_main.xlsx'
mainData = pd.read_excel(mainFileName)

subFileName = 'student_sub.xlsx'
subData = pd.read_excel(subFileName)

mainDataArr = mainData.to_numpy()
subDataArr = subData.to_numpy()

# 서브 학생수를 최소 몇으로 맞춰야 하는지 구하기
minSubStudent = (teamSize - 1) * len(mainDataArr) # 16 을 맞춰야하는데 현재 서브데이터는 8.
loopCount = math.ceil(minSubStudent / len(subDataArr))

resultSubStudent = []
# 서브 학생수 이어 붙이기 작업 시작
for i in range(loopCount):
    for j in range(len(subDataArr)):
        resultSubStudent.append(subDataArr[j])

resultArr = []
for i in range(len(mainDataArr)):
    resultArr.append(mainDataArr[i])
    endRange = (i + 1) * (teamSize - 1) - 1
    startRange = endRange - (teamSize - 2)

    for j in range(startRange, endRange + 1):
        resultArr.append(resultSubStudent[j])


resultDf = pd.DataFrame(resultArr)


resultDf.to_excel('pandas_simple.xlsx',
                  sheet_name="Sheet1",
                  startrow=0,
                  startcol=0,
                  header=["이름", "전화번호", "주소", "생일", "과목명"],
                  index=False)
